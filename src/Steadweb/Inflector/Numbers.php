<?php

namespace Steadweb\Inflector;

class Numbers
{
	/**
	 * @var float
	 */
	protected $value = null;
	
	/**
	 * @var bool
	 */
	protected $return_if_zero = null;

	/**
	 * @var array
	 */
	protected static $dictionary = array(
	0					=> 'zero',
	1					=> 'one',
	2					=> 'two',
	3					=> 'three',
	4					=> 'four',
	5					=> 'five',
	6					=> 'six',
	7					=> 'seven',
	8					=> 'eight',
	9					=> 'nine',
	10					=> 'ten',
	11					=> 'eleven',
	12					=> 'twelve',
	13					=> 'thirteen',
	14					=> 'fourteen',
	15					=> 'fifteen',
	16					=> 'sixteen',
	17					=> 'seventeen',
	18					=> 'eighteen',
	19					=> 'nineteen',
	20					=> 'twenty',
	30					=> 'thirty',
	40					=> 'forty',
	50					=> 'fifty',
	60					=> 'sixty',
	70					=> 'seventy',
	80					=> 'eighty',
	90					=> 'ninety',
	100					=> 'hundred',
	1000				=> 'thousand',
	1000000				=> 'million',
	1000000000			=> 'billion',
	1000000000000		=> 'trillion',
	1000000000000000	=> 'quadrillion',
	1000000000000000000	=> 'quintillion'
	);

	/**
	 * @var array
	 */
	protected static $symbols = array(
		'hyphen'		=> '-',
		'conjunction'	=> ' and ',
		'separator'		=> ', ',
		'negative'		=> 'negative ',
		'decimal'		=> ' point '
	);

	/**
	 * Constructor which accepts a number value only. Set $this->number
	 * to whatever is passed. Throws an exception if the param passed 
	 * isn't numeric.
	 *
	 * @param 	int	$number
	 * @throws 	UnexpectedValueException
	 */
	public function __construct($number = 0, $return_if_zero = true)
	{
		$this->return_if_zero = $return_if_zero;
		
		if ( (! is_numeric($number) || (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX)) && ! $supress_errors) {
			throw new \UnexpectedValueException ('Steadweb\Inflector\Numbers only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX);
		}
		
		$this->number = $number;
	}
	
	/**
	 * Methods to convert number into words. Utilizes dictonary defined.
	 * If number / dictonary aren't passed, defaults are used.
	 *
	 * All credit goes to https://github.com/19ft/NFNumberToWord/blob/master/Module.php#L54
	 * Literally made this compatible with composer and PSR-0
	 *
	 * @param null	$number
	 * @param array	$dictounary
	 * @param bool	$ucfirst
	 *
	 * @returns string
	 */
	public function toWords($ucfirst = false, $number = null, $dictionary = array())
	{
		// Only if the constructor passed return_if_zero as false
		// and the number is 0 should we return null.
		if($this->number == 0 && ! $this->return_if_zero)
		{
			return null;
		}
	
		// Default config values
		$number = $number ? $number : $this->number;
		$dictionary = $dictionary ? $dictionary : static::$dictionary;
		$hyphen      = static::$symbols['hyphen'];
		$conjunction = static::$symbols['conjunction'];
		$separator   = static::$symbols['separator'];
		$negative    = static::$symbols['negative'];
		$decimal     = static::$symbols['decimal'];

		if ($number < 0) {
			return $negative . $this->toWords($ucfirst, abs($number));
		}

		$string = $fraction = null;

		if (strpos($number, '.') !== false) {
			list($number, $fraction) = explode('.', $number);
		}
        
		switch (true) {
			case $number < 21:
				$string = $dictionary[$number];
				break;
			case $number < 100:
				$tens   = ((int) ($number / 10)) * 10;
				$units  = $number % 10;
				$string = $dictionary[$tens];
				if ($units) {
					$string .= $hyphen . $dictionary[$units];
				}
				break;
			case $number < 1000:
				$hundreds  = $number / 100;
				$remainder = $number % 100;
				$string = $dictionary[$hundreds] . ' ' . $dictionary[100];
				if ($remainder) {
					$string .= $conjunction . $this->toWords($ucfirst, $remainder);
				}
				break;
			default:
				$baseUnit = pow(1000, floor(log($number, 1000)));
				$numBaseUnits = (int) ($number / $baseUnit);
				$remainder = $number % $baseUnit;
				$string = $this->toWords($ucfirst, $numBaseUnits) . ' ' . $dictionary[$baseUnit];
				if ($remainder) {
					$string .= $remainder < 100 ? $conjunction : $separator;
					$string .= $this->toWords($ucfirst, $remainder);
				}
				break;
		}

		if (null !== $fraction && is_numeric($fraction)) {
			$string .= $decimal;
			$words = array();
			foreach (str_split((string) $fraction) as $number) {
				$words[] = $dictionary[$number];
			}
			$string .= implode(' ', $words);
		}
		
		return $ucfirst ? ucfirst($string) : $string;
	}
	
	/**
	 * Magic get method to get protected properties.
	 * Otherwise, return null.
	 *
	 * @returns mixed
	 */
	public function __get($value)
	{
		if(isset($this->{$value}))
		{
			return $this->{$value};
		}
		
		return null;
	}
	
	/**
	 * Default stringify object using toWords method.
	 *
	 * @returns string
	 */
	public function __toString()
	{
		return $this->toWords();
	}
}